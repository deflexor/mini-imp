use strict;
use Data::Dumper;
use Scalar::Lazy;
$Data::Dumper::Terse=1;

package lexer;

our $RESERVED = 'RESERVED';
our $INT      = 'INT';
our $ID       = 'ID';

my $imp_token_exprs = [
    ['[ \n\t]+',              undef],
    ['#[^\n]*',               undef],
    ['\:=',                   $RESERVED],
    ['\(',                    $RESERVED],
    ['\)',                    $RESERVED],
    [';',                     $RESERVED],
    ['\+',                    $RESERVED],
    ['-',                     $RESERVED],
    ['\*',                    $RESERVED],
    ['/',                     $RESERVED],
    ['<',                     $RESERVED],
    ['<=',                    $RESERVED],
    ['>',                     $RESERVED],
    ['>=',                    $RESERVED],
    ['=',                     $RESERVED],
    ['!=',                    $RESERVED],
    ['and',                   $RESERVED],
    ['or',                    $RESERVED],
    ['not',                   $RESERVED],
    ['if',                    $RESERVED],
    ['then',                  $RESERVED],
    ['else',                  $RESERVED],
    ['while',                 $RESERVED],
    ['do',                    $RESERVED],
    ['end',                   $RESERVED],
    ['[0-9]+',                $INT],
    ['[A-Za-z][A-Za-z0-9_]*', $ID],
];

sub lex {
	my ($characters, $token_exprs) = @_;
	$token_exprs ||= $imp_token_exprs;
    my $pos = 0;
    my @tokens;
    while($pos < length($characters)) {
        my $match;
        for my $token_expr (@$token_exprs) {
            my ($pat, $tag) = @$token_expr;
            $match = substr($characters, $pos) =~ qr/^$pat/;
            if($match) {
                if($tag) {
                    push @tokens, [$&, $tag];
                }
            	$pos += length($&);
                last;
            }
        }
        if (!$match) {
            die('Illegal character: "' . substr($characters, $pos-1, 1) . '"');
        }
    }
    return \@tokens;
}

1;
package main;

my $file = $ARGV[0] or die "useage: $0 source_file";
open my $fh, "<", $file or die "open '$file':$!";
my $txt = do { local $/; <$fh> };

my $tokens = lexer::lex($txt);

#my $parser = mk_concat(mk_concat(mk_tag($lexer::INT), mk_reserved('+', $lexer::RESERVED)), mk_tag($lexer::INT));
my $parser = statements();
my $ast_result = $parser->(0, @$tokens);
#warn(Dumper($ast_result));

my %env;
$ast_result->val->evaluate(\%env);

warn "Final variable values:\n";
warn(Dumper(\%env));

#
# Combinators
#

sub num { mk_process(mk_tag($lexer::INT), sub { int($_[0]) }) }
sub id { mk_tag($lexer::ID) }
sub keyword { mk_reserved($_[0], $lexer::RESERVED) }

sub statements() {
	mk_process( mk_rep_delim( stmt(), keyword(';') ), sub {
			ast::compound(@{$_[0]});
		});
}

sub stmt {
    mk_alt1(assign_stmt(), while_stmt());
    #mk_alt1(assign_stmt(), if_stmt(), while_stmt());
}

sub  assign_stmt {
    mk_process( mk_concat1(id(),  keyword(':='),  aexp()), sub {
    		my ($name, undef, $exp) = @{$_[0]};
    		ast::assign($name, $exp);
    });
}

sub while_stmt {
    my $p1 = mk_concat1(keyword('while'),  bexp(), keyword('do'), lazy { statements() }, keyword('end') );
    mk_process($p1, sub {
    		my (undef, $cond, undef, $body, undef) = @{$_[0]};
			ast::whilestmt($cond, $body);
    	});
}

# Arithmetic expression parsers
sub aexp {
	my $comp = sub {
		my ($op, $l, $r) = @_;
		ast::binopaexp($op, $l, $r);
	};
	my $p1 = mk_exp(aexp_term(), mk_alt1(map { keyword($_) } qw(* /)), $comp);
	my $p2 = mk_exp($p1, mk_alt1(map { keyword($_) } qw(+ -)), $comp);
	$p2;
}

sub aexp_term {
    mk_alt1(aexp_value(), lazy { aexp_group() });
}

sub aexp_group {
    mk_process( mk_concat1( keyword('('), aexp(), keyword(')') ),
    	\&process_group
    );
}
           
sub aexp_value {
    mk_alt1(
        mk_process(num(), sub { ast::intaexp($_[0]) }),
        mk_process(id(),  sub { ast::varaexp($_[0]) })
       );
}

# Boolean expression parsers
sub bexp {
	my $comp = sub {
		my ($op, $l, $r) = @_;
		if($op eq 'and') {
			return ast::andbexp($l, $r);
		}
		elsif($op eq 'or') {
			return ast::orbexp($l, $r);
		}
		die "unknown operator: '$op'";
	};
	my $p1 = mk_exp(bexp_term(), keyword('and'), $comp);
	my $p2 = mk_exp($p1, keyword('or'), $comp);
	$p2;
}

sub bexp_term {
    mk_alt1( bexp_not(), bexp_relop(), bexp_group());
}

sub bexp_not {
    mk_process(
    	mk_concat1( keyword('not') , lazy { bexp_term() } ),
    	sub { warn Dmper(\@_); ast::notbexp($_[1]) }
    );
}

sub bexp_relop {
    my @relops = ('<', '<=', '>', '>=', '=', '!=');
    mk_process( mk_concat1(aexp(),  mk_alt1(map { keyword($_) } @relops), aexp()),
    	\&process_relop
    );
}

sub bexp_group {
    mk_process( mk_concat1( keyword('('), lazy { bexp() }, keyword(')') ),
    	\&process_group
    );
}

sub process_relop {
	my ($left, $op, $right) = @{$_[0]};
    ast::relopbexp($op, $left, $right);
}

sub process_group {
	my (undef, $v, undef) = @{$_[0]};
	$v
}

# useful classes
# result { val, pos }
sub result::new { my ($c, $val, $pos) = @_; bless [ $val, $pos ], $c }
sub result::val { shift->[0] }
sub result::pos { shift->[1] }

#
# statement and expressions classes
#
sub ast::intaexp { bless \$_[0], 'IntAexp' }
sub IntAexp::evaluate {
	${$_[0]};
}
sub ast::varaexp { bless \$_[0], 'VarAexp' }
sub VarAexp::evaluate {
	my ($self, $env) = @_;
	my $name =  $$self;
	$env->{ $name } || 0;
}
sub ast::binopaexp {
	my ($op, $left, $right) = @_;
	bless [ $left, $op, $right ], 'BinopAexp';
}
sub BinopAexp::evaluate {
	my ($self, $env) = @_;
	my ($left, $op, $right) = @$self;
	my $left_val = $left->evaluate($env);
	my $right_val = $right->evaluate($env);
	eval "$left_val $op $right_val";
}
sub ast::relopbexp {
	my ($op, $left, $right) = @_;
	bless [ $left, $op, $right ], 'RelopBexp';
}

sub RelopBexp::evaluate {
	my ($self, $env) = @_;
	my ($left, $op, $right) = @$self;
	my $left_val = $left->evaluate($env);
	my $right_val = $right->evaluate($env);
	$op = 'eq' if $op eq '=';
	eval "$left_val $op $right_val";
}



sub ast::compound {
	bless \@_, 'CompoundStmt';
}
sub CompoundStmt::evaluate {
	my ($self, $env) = @_;
	$_->evaluate($env) for @$self;
}

sub ast::assign {
	my ($name, $expr) = @_;
	bless [$name, $expr], 'AssignStmt';
}
sub AssignStmt::evaluate {
	my ($self, $env) = @_;
	my $result =  $self->[1]->evaluate($env);
	$env->{ $self->[0] } = $result;
}
sub ast::whilestmt {
	my ($cond, $body) = @_;
	bless [$cond, $body], 'WhileStmt';
}
sub WhileStmt::evaluate {
	my ($self, $env) = @_;
	my ($cond, $body) = @$self;
	my $cond_val =  $cond->evaluate($env);
	while($cond_val) {
		$body->evaluate($env);
		$cond_val =  $cond->evaluate($env);
	}
}

#
# Base parsers
#
sub mk_alt1 {
	my (@parsers) = @_;
	sub {
		my ($pos, @tokens) = @_;
		my $r;
		for(@parsers) {
			$r = $_->($pos, @tokens) and last;
		}
		$r;
	}
}

sub mk_alt {
	my ($left, $right) = @_;
	sub {
		my ($pos, @tokens) = @_;
		$left->($pos, @tokens) || $right->($pos, @tokens);
	}
}

# optional
sub mk_opt {
	my ($parser) = @_;
	sub {
		my ($pos, @tokens) = @_;
		$parser->($pos, @tokens) || result->new( undef, $pos );
	}
}

# repeat
sub mk_rep {
	my ($parser) = @_;
	sub {
		my ($pos, @tokens) = @_;
		my $res = $parser->($pos, @tokens);
		my @results;
		while($res) {
			push @results, $res->val;
			$pos = $res->pos;
			$res = $parser->($pos, @tokens);
		}
		result->new( \@results, $pos );
	}
}

# repeat with delimiter
sub mk_rep_delim {
	my ($parser, $delim_p) = @_;
	sub {
		my ($pos, @tokens) = @_;
		my $res = $parser->($pos, @tokens);
		my @results;
		while($res) {
			push @results, $res->val;
			$pos = $res->pos;
			my $res1 = $delim_p->($pos, @tokens) or last;
			$res = $parser->($res1->pos, @tokens);
		}
		result->new( \@results, $pos );
	}
}

# expression
sub mk_exp {
	my ($parser, $delim_p, $compose) = @_;
	sub {
		my ($pos, @tokens) = @_;
		my $res = $parser->($pos, @tokens);
		my $next_parser = mk_process( mk_concat1( $delim_p, $parser ), sub {
			my ($delim, $right) = @{$_[0]};
			$compose->($delim, $res->val, $right);
		});
		while(my $res1 = $next_parser->($res->pos, @tokens)) {
			$res = $res1;
		}
		$res;
	}
}

sub mk_process {
	my ($parser, $f) = @_;
	sub {
		my ($pos, @tokens) = @_;
		my $res = $parser->($pos, @tokens) or return undef;
		result->new( $f->( $res->val ), $res->pos );
	}
}

sub mk_concat1 {
	my (@parsers) = @_;
	sub {
		my ($pos, @tokens) = @_;
		my @result;
		for(@parsers) {
			my $r = $_->($pos, @tokens) or return undef;
			$pos = $r->pos;
			push @result, $r->val;
		}
		result->new( \@result, $pos );
	}
}

sub mk_concat {
	my ($left, $right) = @_;
	sub {
		my ($pos, @tokens) = @_;
		my $left_r = $left->($pos, @tokens);
		if($left_r) {
			my $right_r = $right->($left_r->pos(), @tokens);
			if($right_r) {
				return result->new( [ $left_r->val, $right_r->val ], $right_r->pos() );
			}
		}
		undef;
	}
}

sub mk_reserved {
	my ($val, $tag) = @_;
	sub {
		my ($pos, @tokens) = @_;
		my $curtok = $tokens[$pos];
		($pos < scalar(@tokens) && $curtok->[0] eq $val && $curtok->[1] eq $tag) ?
			result->new($curtok->[0], $pos + 1) :
			undef;
	}
}

sub mk_tag {
	my ($tag) = @_;
	sub {
		my ($pos, @tokens) = @_;
		my $curtok = $tokens[$pos];
		($pos < scalar @tokens && $curtok->[1] eq $tag) ? result->new($curtok->[0], $pos + 1) : undef;
	}
}

